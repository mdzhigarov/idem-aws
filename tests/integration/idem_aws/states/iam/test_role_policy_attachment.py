import copy
import uuid

import pytest


@pytest.mark.asyncio
async def test_role_policy_attachment(hub, ctx, aws_iam_role):
    attach_role_policy_temp_name = "idem-test-role-policy-attachment-" + str(
        uuid.uuid4()
    )
    role_name = aws_iam_role.get("name")
    policy_arn = hub.tool.aws.arn_utils.build(
        service="iam",
        account_id="aws",
        resource="policy/ReadOnlyAccess",
    )

    # Attach an IAM managed policy to an IAM role with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        test_ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_arn == resource.get("policy_arn")
    assert "Would create" in str(ret["comment"])

    # Attach an IAM managed policy to non existing IAM role with test flag
    # Should not fail since the role may be created in the same 'test' run
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        test_ctx,
        name=attach_role_policy_temp_name,
        role_name="non_present_role",
        policy_arn=policy_arn,
    )
    assert ret["result"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert "Would create" in str(ret["comment"])

    # Attach an IAM managed policy to an IAM role
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_arn == resource.get("policy_arn")

    # Create again
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "already exists" in str(ret["comment"]), ret["comment"]
    resource = ret.get("new_state")
    assert policy_arn == resource.get("policy_arn")

    # Describe attached role policies
    describe_ret = await hub.states.aws.iam.role_policy_attachment.describe(ctx)
    assert f"{role_name}-{policy_arn}" in describe_ret

    # Detach an IAM managed policy to an IAM role with test flag
    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        test_ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert "Would detach" in str(ret["comment"])

    # Detach an IAM managed policy to an IAM role
    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Detach again an IAM managed policy to an IAM role
    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
