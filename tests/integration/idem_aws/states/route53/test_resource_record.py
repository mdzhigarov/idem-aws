import random
import string
from collections import ChainMap
from copy import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_resource_record(hub, ctx, aws_hosted_zone):
    random_str = "".join(random.choice(string.ascii_lowercase) for _ in range(5))
    # Test present() with 'test' flag.
    name = (
        f'idem-test-route53-resource-record-{random_str}.{aws_hosted_zone.get("name")}.'
    )
    resource_records = ["192.168.0.2", "192.168.0.3"]
    alias_target = {
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "dns_name": f"{name}",
        "evaluate_target_health": False,
    }

    try:
        ctx["test"] = True
        resource_record = await hub.states.aws.route53.resource_record.present(
            ctx,
            name=name,
            hosted_zone_id=aws_hosted_zone.get("resource_id"),
            record_type="A",
            ttl=30,
            resource_records=resource_records,
        )
    finally:
        ctx["test"] = False

    resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{name}", "A"
    )
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        resource_record,
        comment=hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=None,
        old_state=None,
        new_state={
            "name": f"{name}",
            "ttl": 30,
            "hosted_zone_id": aws_hosted_zone.get("resource_id"),
            "resource_id": resource_id,
            "record_type": "A",
            "resource_records": resource_records,
        },
    )

    # Test present() will create resource record successfully.
    resource_record = await hub.states.aws.route53.resource_record.present(
        ctx,
        name=name,
        hosted_zone_id=aws_hosted_zone.get("resource_id"),
        record_type="A",
        ttl=30,
        resource_records=resource_records,
    )

    resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{name}", "A"
    )
    new_state = {
        "name": f"{name}",
        "ttl": 30,
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": resource_id,
        "record_type": "A",
        "resource_records": resource_records,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        resource_record,
        comment=hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=new_state,
        old_state=None,
        new_state=new_state,
    )

    # Test present() will create alias resource record set successfully.
    alias_name = f'*.idem-test-route53-resource-record-{random_str}-alias.{aws_hosted_zone.get("name")}.'
    alias_resource_record = await hub.states.aws.route53.resource_record.present(
        ctx,
        name=alias_name,
        hosted_zone_id=aws_hosted_zone.get("resource_id"),
        record_type="A",
        alias_target=alias_target,
    )

    alias_resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{alias_name}", "A"
    )
    new_state = {
        "name": f"{alias_name}",
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": alias_resource_id,
        "record_type": "A",
        "alias_target": alias_target,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        alias_name,
        alias_resource_record,
        comment=hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.route53.resource_record", name=alias_name
        ),
        true_state=new_state,
        old_state=None,
        new_state=new_state,
    )

    # test no op if no changes are made
    resource_record_alias_no_op = await hub.states.aws.route53.resource_record.present(
        ctx,
        name=alias_name,
        hosted_zone_id=aws_hosted_zone.get("resource_id"),
        record_type="A",
        alias_target=alias_target,
    )

    resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{alias_name}", "A"
    )
    new_state = {
        "name": f"{alias_name}",
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": resource_id,
        "record_type": "A",
        "alias_target": alias_target,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        alias_name,
        resource_record_alias_no_op,
        comment=(
            f"No changes would be made for aws.route53.resource_record {alias_name}",
        ),
        true_state=new_state,
        old_state=resource_record_alias_no_op["old_state"],
        new_state=resource_record_alias_no_op["new_state"],
    )
    assert resource_record_alias_no_op["result"], resource_record_alias_no_op["comment"]
    assert (
        resource_record_alias_no_op["old_state"]
        and resource_record_alias_no_op["new_state"]
    )

    # Test describe() will look-up resource records successfully.
    response = await hub.states.aws.route53.resource_record.describe(ctx)

    for rr in [resource_record, alias_resource_record]:
        assert rr["new_state"]["resource_id"] in response

        assert aws_hosted_zone.get("resource_id") == rr["new_state"].get(
            "hosted_zone_id"
        )
        assert "A" == rr["new_state"].get("record_type")

        state = response[rr["new_state"]["resource_id"]][
            "aws.route53.resource_record.present"
        ]
        assert hub.tool.aws.route53.resource_record_utils.same_states(
            dict(ChainMap(*state)), rr["new_state"]
        )

    # Test present() update resource record with 'test' flag.
    updated_resource_records = copy(resource_records)
    updated_resource_records.append("192.168.0.4")

    try:
        ctx["test"] = True
        resource_record = await hub.states.aws.route53.resource_record.present(
            ctx,
            name=name,
            hosted_zone_id=aws_hosted_zone.get("resource_id"),
            record_type="A",
            ttl=300,
            resource_records=updated_resource_records,
        )
    finally:
        ctx["test"] = False

    resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{name}", "A"
    )
    old_state = {
        "name": f"{name}",
        "ttl": 30,
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": resource_id,
        "record_type": "A",
        "resource_records": resource_records,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        resource_record,
        comment=hub.tool.aws.comment_utils.would_update_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=old_state,
        old_state=old_state,
        new_state={
            "name": f"{name}",
            "ttl": 300,
            "hosted_zone_id": aws_hosted_zone.get("resource_id"),
            "resource_id": resource_id,
            "record_type": "A",
            "resource_records": updated_resource_records,
        },
    )

    # Test present() will update resource record successfully.
    resource_record = await hub.states.aws.route53.resource_record.present(
        ctx,
        name=name,
        hosted_zone_id=aws_hosted_zone.get("resource_id"),
        record_type="A",
        ttl=300,
        resource_records=updated_resource_records,
    )

    resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{name}", "A"
    )
    new_state = {
        "name": f"{name}",
        "ttl": 300,
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": resource_id,
        "record_type": "A",
        "resource_records": updated_resource_records,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        resource_record,
        comment=hub.tool.aws.comment_utils.update_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=new_state,
        old_state={
            "name": f"{name}",
            "ttl": 30,
            "hosted_zone_id": aws_hosted_zone.get("resource_id"),
            "resource_id": resource_id,
            "record_type": "A",
            "resource_records": resource_records,
        },
        new_state=new_state,
    )

    # test no op if no changes are made
    resource_record = await hub.states.aws.route53.resource_record.present(
        ctx,
        name=name,
        hosted_zone_id=aws_hosted_zone.get("resource_id"),
        record_type="A",
        ttl=300,
        resource_records=updated_resource_records,
    )

    resource_id = hub.tool.aws.route53.resource_record_utils.compose_resource_id(
        aws_hosted_zone.get("resource_id"), f"{name}", "A"
    )
    new_state = {
        "name": f"{name}",
        "ttl": 300,
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": resource_id,
        "record_type": "A",
        "resource_records": updated_resource_records,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        resource_record,
        comment=(f"No changes would be made for aws.route53.resource_record {name}",),
        true_state=new_state,
        old_state=resource_record["old_state"],
        new_state=resource_record["new_state"],
    )
    assert resource_record["result"], resource_record["comment"]
    assert resource_record["old_state"] and resource_record["new_state"]

    # Test absent() with 'test' flag.
    resource_record["new_state"]
    try:
        ctx["test"] = True
        response = await hub.states.aws.route53.resource_record.absent(
            ctx, name=name, resource_id=resource_id
        )
    finally:
        ctx["test"] = False

    old_state = {
        "name": f"{name}",
        "ttl": 300,
        "hosted_zone_id": aws_hosted_zone.get("resource_id"),
        "resource_id": resource_id,
        "record_type": "A",
        "resource_records": updated_resource_records,
    }
    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        response,
        comment=hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=old_state,
        old_state=old_state,
        new_state=None,
    )

    # Test absent() will delete resource record successfully.
    response = await hub.states.aws.route53.resource_record.absent(
        ctx, name=name, resource_id=resource_id
    )

    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        response,
        comment=hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=None,
        old_state={
            "name": f"{name}",
            "ttl": 300,
            "hosted_zone_id": aws_hosted_zone.get("resource_id"),
            "resource_id": resource_id,
            "record_type": "A",
            "resource_records": updated_resource_records,
        },
        new_state=None,
    )

    # Test absent() for deleted resource record.
    response = await hub.states.aws.route53.resource_record.absent(
        ctx, name=name, resource_id=resource_id
    )

    await hub.tool.resource_record_test_util.do_asserts(
        ctx,
        aws_hosted_zone.get("resource_id"),
        name,
        response,
        comment=hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.route53.resource_record", name=name
        ),
        true_state=None,
        old_state=None,
        new_state=None,
    )

    # Test present() will fail when there is a discrepancy between the resource_id and the properties that are used
    # to build this resource_id. Resource Record in aws does not have its own resource id and we compose one formatted
    # as <hosted_zone_id>_<name>_<record_type>. In present() is checked that resource_id and these fields match.
    resource_record = await hub.states.aws.route53.resource_record.present(
        ctx,
        name=name,
        hosted_zone_id=aws_hosted_zone.get("resource_id"),
        resource_id="wrong_resource_id",
        record_type="A",
        ttl=30,
        resource_records=resource_records,
    )
    assert not resource_record["result"]
    assert (
        f"aws.route53.resource_record resource_id wrong_resource_id is not composed of "
        f'{aws_hosted_zone.get("resource_id")}, '
        f"{name} and A" in resource_record["comment"]
    )
