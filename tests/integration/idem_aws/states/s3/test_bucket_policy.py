import copy
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_policy(hub, ctx, aws_s3_bucket):
    # Create bucket policy
    bucket_name = aws_s3_bucket.get("name")
    bucket_policy_name = f"{bucket_name}-policy"
    policy_to_add = (
        '{"Version":"2012-10-17","Statement":[{"Sid":"PublicReadGetObject","Effect":"Allow",'
        '"Principal":"*","Action":"s3:GetObject","Resource":"arn:aws:s3:::'
        + bucket_name
        + '/*"}]} '
    )

    # Create bucket policy with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.s3.bucket_policy.present(
        test_ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=policy_to_add,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.s3.bucket_policy '{bucket_policy_name}'" in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert bucket_policy_name == resource.get("name")
    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), policy_to_add
    )
    assert bucket_name == resource.get("bucket")

    # Create real bucket policy
    ret = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=policy_to_add,
    )
    assert ret["result"], ret["comment"]
    assert f"Bucket policy '{bucket_policy_name}' created." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert bucket_policy_name == resource.get("name")
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), policy_to_add
    )
    assert bucket_name == resource.get("bucket")

    # Describe s3 bucket policies
    describe_ret = await hub.states.aws.s3.bucket_policy.describe(ctx)
    assert bucket_policy_name in describe_ret

    # Verify that describe output format is correct
    assert "aws.s3.bucket_policy.present" in describe_ret.get(bucket_policy_name)
    described_resource = describe_ret.get(bucket_policy_name).get(
        "aws.s3.bucket_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "bucket" in described_resource_map
    assert bucket_name == described_resource_map["bucket"]
    assert "policy" in described_resource_map
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        described_resource_map["policy"], policy_to_add
    )

    # Update bucket policy with invalid bucket policy name
    invalid_bucket_policy_name = f"Invalid-{bucket_policy_name}"
    ret = await hub.states.aws.s3.bucket_policy.present(
        test_ctx,
        name=invalid_bucket_policy_name,
        bucket=bucket_name,
        policy=policy_to_add,
        resource_id=resource_id,
    )
    assert not ret["result"], ret["comment"]
    assert (
        f"Incorrect aws.s3.bucket_policy name: {invalid_bucket_policy_name}. Expected name {bucket_name}-policy"
        in ret["comment"]
    )

    # Update bucket policy with test flag and no change in policy
    ret = await hub.states.aws.s3.bucket_policy.present(
        test_ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=policy_to_add,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), policy_to_add
    )
    assert f"No change in policy." in ret["comment"]

    updated_policy = (
        '{"Version":"2012-10-17","Statement":[{"Sid":"AddPerm","Effect":"Allow","Principal":"*",'
        '"Action":"s3:GetObject","Resource":"arn:aws:s3:::' + bucket_name + '/*"}]} '
    )

    # Update bucket policy with test flag
    ret = await hub.states.aws.s3.bucket_policy.present(
        test_ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=updated_policy,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), updated_policy
    )
    assert f"Would update aws.s3.bucket_policy '{bucket_policy_name}'" in ret["comment"]

    # Update bucket policy with real-aws and no change in policy
    ret = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=policy_to_add,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), policy_to_add
    )
    assert f"No change in policy." in ret["comment"]

    # Update real bucket policy
    ret = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=updated_policy,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert f"Policy updated successfully" in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert bucket_policy_name == resource.get("name")
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), updated_policy
    )
    assert bucket_name == resource.get("bucket")

    # Update bucket policy with real-aws with different order of statements
    # It should be no-op. Changing the order of statements should not update the policy
    order_modified_policy = (
        '{"Statement":[{"Effect":"Allow","Sid":"AddPerm","Principal":"*","Action":"s3:GetObject",'
        '"Resource":"arn:aws:s3:::' + bucket_name + '/*"}],"Version":"2012-10-17"} '
    )

    ret = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=order_modified_policy,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    hub.tool.aws.state_comparison_utils.are_policies_equal(
        resource.get("policy"), order_modified_policy
    )
    assert f"No change in policy." in ret["comment"]

    # Delete bucket policy with no resource_id
    ret = await hub.states.aws.s3.bucket_policy.absent(
        test_ctx, name=bucket_policy_name, bucket=bucket_name
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]
    assert (
        f"aws.s3.bucket_policy '{bucket_policy_name}' already absent" in ret["comment"]
    )

    # Delete bucket policy with invalid resource_id
    invalid_resource_id = f"Invalid-{resource_id}"
    ret = await hub.states.aws.s3.bucket_policy.absent(
        test_ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]
    assert f"Incorrect aws.s3.bucket name: '{bucket_name}'" in ret["comment"]

    # Delete bucket policy with test flag
    ret = await hub.states.aws.s3.bucket_policy.absent(
        test_ctx, name=bucket_policy_name, bucket=bucket_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Would delete aws.s3.bucket_policy '{bucket_policy_name}'" in ret["comment"]

    # Delete real bucket policy
    ret = await hub.states.aws.s3.bucket_policy.absent(
        ctx, name=bucket_policy_name, bucket=bucket_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Deleted policy '{bucket_policy_name}' for bucket '{bucket_name}'"
        in ret["comment"]
    )

    # Deleting the same bucket policy (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.s3.bucket_policy.absent(
        ctx, name=bucket_policy_name, bucket=bucket_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]
    assert (
        f"aws.s3.bucket_policy '{bucket_policy_name}' already absent" in ret["comment"]
    )

    # if bucket policy is not present, and we pass resource_id it should say bucket policy does not exist
    ret = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_name,
        bucket=bucket_name,
        policy=order_modified_policy,
        resource_id=resource_id,
    )
    assert not ret["result"], ret["comment"]
    assert f"NoSuchBucketPolicy" in str(ret["comment"])
    assert f"The bucket policy does not exist" in str(ret["comment"])
