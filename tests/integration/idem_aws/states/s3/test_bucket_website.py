import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "test_bucket_name",
    "website_configuration": {
        "ErrorDocument": {"Key": "index.html"},
        "IndexDocument": {"Suffix": "index.html"},
    },
    "checksum_algorithm": "SHA256",
}


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="invalid-get-resource-id")
async def test_get_invalid_resource_id(hub, ctx, aws_s3_bucket, __test):
    global PARAMETER
    bucket = aws_s3_bucket.get("name")
    PARAMETER["bucket"] = bucket
    ret = await hub.exec.aws.s3.bucket_website.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=aws_s3_bucket.get("name")
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            "aws.s3.bucket_website",
            PARAMETER["name"],
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present", depends=["invalid-get-resource-id"])
async def test_present(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    ret = await hub.states.aws.s3.bucket_website.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]

    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.s3.bucket_website",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.s3.bucket_website",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_bucket_website(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.s3.bucket_website.describe(ctx)
    # Verify that describe output format is correct
    assert "aws.s3.bucket_website.present" in describe_ret.get(PARAMETER["resource_id"])
    describe_params = describe_ret.get(PARAMETER["resource_id"]).get(
        "aws.s3.bucket_website.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert PARAMETER.get("bucket") == described_resource_map.get("bucket")
    assert PARAMETER.get("website_configuration") == described_resource_map.get(
        "website_configuration"
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="modify_bucket_website_attributes", depends=["present"])
async def test_modify_attributes(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["website_configuration"]["ErrorDocument"]["Key"] = "error.html"

    ret = await hub.states.aws.s3.bucket_website.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.s3.bucket_website",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.s3.bucket_website",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_bucket_website(hub, ctx, ret["old_state"], PARAMETER)
    assert_bucket_website(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_exec_get", depends=["modify_bucket_website_attributes"]
)
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.s3.bucket_website.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_bucket_website(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_website.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_bucket_website(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.s3.bucket_website",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.s3.bucket_website",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_website.absent(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket_website",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


def assert_bucket_website(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("bucket") == resource.get("bucket")
    assert parameters.get("website_configuration") == resource.get(
        "website_configuration"
    )
