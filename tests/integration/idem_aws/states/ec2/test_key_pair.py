import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-ec2-key-pair-" + str(int(time.time())),
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "The most properties of key pair resource are not returned when the "
    "resource is created in localstack anyway so there is no point in testing them.",
)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test

    PARAMETER["public_key"] = (
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP"
        "/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4"
        "+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO"
        "/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9"
        "+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 "
        "email@example.com"
    )
    PARAMETER["tags"] = {"idem-test-key-name": PARAMETER["name"]}

    ret = await hub.states.aws.ec2.key_pair.present(ctx, **PARAMETER)

    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.ec2.key_pair", name=PARAMETER["name"]
            )
            == ret["comment"]
        )
        assert "resource_id_known_after_present" == resource.get("resource_id")
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        PARAMETER["public_key"] = resource["public_key"]
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.ec2.key_pair", name=PARAMETER["name"]
            )
            == ret["comment"]
        )

    assert not ret.get("old_state") and ret.get("new_state")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "The most properties of key pair resource are not returned when the "
    "resource is created in localstack anyway so there is no point in testing them.",
)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.key_pair.describe(ctx)
    resource_name = PARAMETER["name"]
    assert resource_name in describe_ret
    # Verify that describe output format is correct
    assert "aws.ec2.key_pair.present" in describe_ret[resource_name]
    described_resource = describe_ret[resource_name].get("aws.ec2.key_pair.present")
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert PARAMETER["resource_id"] == described_resource_map["resource_id"]
    assert PARAMETER["tags"] == described_resource_map["tags"]
    assert PARAMETER["public_key"] == described_resource_map["public_key"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "The most properties of key pair resource are not returned when the "
    "resource is created in localstack anyway so there is no point in testing them.",
)
@pytest.mark.dependency(name="add_tags", depends=["describe"])
async def test_add_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tags"].update(
        {
            f"idem-test-key-{str(int(time.time()))}": f"idem-test-value-{str(int(time.time()))}",
        }
    )

    ret = await hub.states.aws.ec2.key_pair.present(ctx, **new_parameter)

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["public_key"] == old_resource["public_key"]
    assert PARAMETER["tags"] == old_resource["tags"]

    resource = ret["new_state"]
    assert new_parameter["tags"] == resource["tags"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["public_key"] == resource["public_key"]

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.ec2.key_pair",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.update_comment(
                "aws.ec2.key_pair",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "The most properties of key pair resource are not returned when the "
    "resource is created in localstack anyway so there is no point in testing them.",
)
@pytest.mark.dependency(name="remove_tags", depends=["add_tags"])
async def test_remove_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    del new_parameter["tags"]["idem-test-key-name"]

    ret = await hub.states.aws.ec2.key_pair.present(ctx, **new_parameter)

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["public_key"] == old_resource["public_key"]
    assert PARAMETER["tags"] == old_resource["tags"]

    resource = ret["new_state"]
    assert new_parameter["tags"] == resource["tags"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["public_key"] == resource["public_key"]

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.ec2.key_pair",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.update_comment(
                "aws.ec2.key_pair",
                PARAMETER["name"],
            )
            == ret["comment"]
        )

    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "The most properties of key pair resource are not returned when"
    " the resource is created in localstack anyway so there is no point in testing them.",
)
@pytest.mark.dependency(name="absent", depends=["remove_tags"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.key_pair.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))
    old_resource = ret["old_state"]
    assert PARAMETER["tags"] == old_resource["tags"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["public_key"] == old_resource["public_key"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.ec2.key_pair", name=PARAMETER["name"]
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.ec2.key_pair", name=PARAMETER["name"]
            )
            == ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "The most properties of key pair resource are not returned when"
    " the resource is created in localstack anyway so there is no point in testing them.",
)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.key_pair.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.key_pair", name=PARAMETER["name"]
        )
        == ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.key_pair.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
