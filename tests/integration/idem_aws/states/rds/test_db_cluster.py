import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_db_cluster(hub, ctx, aws_rds_db_subnet_group):
    # To skip running on localstack as local stack is not able to create the resource, it is working with real aws
    if hub.tool.utils.is_running_localstack(ctx):
        return

    # Create db cluster
    db_cluster_name = "idem-test-db-cluster-" + str(uuid.uuid4())
    engine = "aurora-postgresql"
    master_username = "admin123"
    master_user_password = "abcd1234"
    backup_retention_period = 7
    database_name = "dbname123"
    engine_version = "12.7"
    port = 5432
    preferred_backup_window = "07:41-08:11"
    preferred_maintenance_window = "sat:09:29-sat:09:59"
    storage_encrypted = True
    engine_mode = "provisioned"
    deletion_protection = False
    copy_tags_to_snapshot = True
    db_subnet_group_name = aws_rds_db_subnet_group.get("resource_id")
    tags = {"Name": db_cluster_name}
    cloudwatch_logs_export_configuration = {"EnableLogTypes": ["postgresql"]}
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create DB Cluster with test flags
    ret = await hub.states.aws.rds.db_cluster.present(
        test_ctx,
        name=db_cluster_name,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
        apply_immediately=True,
        allow_major_version_upgrades=True,
        cloudwatch_logs_export_configuration=cloudwatch_logs_export_configuration,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.rds.db_cluster '{db_cluster_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cloudwatch_logs_export_configuration == resource.get(
        "cloudwatch_logs_export_configuration"
    )
    assert True == resource.get("allow_major_version_upgrades")
    assert True == resource.get("apply_immediately")
    assert_db_cluster(
        hub,
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        database_name,
        engine_version,
        port,
        preferred_backup_window,
        preferred_maintenance_window,
        storage_encrypted,
        engine_mode,
        deletion_protection,
        copy_tags_to_snapshot,
        db_subnet_group_name,
        tags,
    )

    # Create DB Cluster
    ret = await hub.states.aws.rds.db_cluster.present(
        ctx,
        name=db_cluster_name,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
        apply_immediately=True,
        allow_major_version_upgrades=True,
        cloudwatch_logs_export_configuration=cloudwatch_logs_export_configuration,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.rds.db_cluster '{db_cluster_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_cluster(
        hub,
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        database_name,
        engine_version,
        port,
        preferred_backup_window,
        preferred_maintenance_window,
        storage_encrypted,
        engine_mode,
        deletion_protection,
        copy_tags_to_snapshot,
        db_subnet_group_name,
        tags,
    )
    resource_id = resource.get("resource_id")

    # Describe db cluster
    describe_ret = await hub.states.aws.rds.db_cluster.describe(ctx)
    assert db_cluster_name in describe_ret
    assert "aws.rds.db_cluster.present" in describe_ret.get(db_cluster_name)
    described_resource = describe_ret[resource_id].get("aws.rds.db_cluster.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_db_cluster(
        hub,
        described_resource_map,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        database_name,
        engine_version,
        port,
        preferred_backup_window,
        preferred_maintenance_window,
        storage_encrypted,
        engine_mode,
        deletion_protection,
        copy_tags_to_snapshot,
        db_subnet_group_name,
        tags,
    )

    # Updating the db_cluster with test flag
    tags.update(
        {
            f"idem-test-db-cluster-key-{str(uuid.uuid4())}": f"idem-test-db-cluster-value-{str(uuid.uuid4())}",
        }
    )
    backup_retention_period = 5
    preferred_backup_window = "08:41-09:11"
    preferred_maintenance_window = "fri:09:29-fri:09:59"
    master_user_password = "test1234"

    ret = await hub.states.aws.rds.db_cluster.present(
        test_ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_cluster(
        hub,
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        database_name,
        engine_version,
        port,
        preferred_backup_window,
        preferred_maintenance_window,
        storage_encrypted,
        engine_mode,
        deletion_protection,
        copy_tags_to_snapshot,
        db_subnet_group_name,
        tags,
    )

    # Updating the db_cluster
    ret = await hub.states.aws.rds.db_cluster.present(
        ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_cluster(
        hub,
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        database_name,
        engine_version,
        port,
        preferred_backup_window,
        preferred_maintenance_window,
        storage_encrypted,
        engine_mode,
        deletion_protection,
        copy_tags_to_snapshot,
        db_subnet_group_name,
        tags,
    )

    # updating the db_cluster with no parameters modified so that no changes should be detected
    ret = await hub.states.aws.rds.db_cluster.present(
        ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    # asserting no changes to be made if no parameter is modified
    assert not ret.get("changes")
    resource = ret.get("new_state")
    assert_db_cluster(
        hub,
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        database_name,
        engine_version,
        port,
        preferred_backup_window,
        preferred_maintenance_window,
        storage_encrypted,
        engine_mode,
        deletion_protection,
        copy_tags_to_snapshot,
        db_subnet_group_name,
        tags,
    )

    # Test deleting tags
    tags.pop("Name", None)
    ret = await hub.states.aws.rds.db_cluster.present(
        ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        database_name=database_name,
        engine_version=engine_version,
        port=port,
        preferred_backup_window=preferred_backup_window,
        preferred_maintenance_window=preferred_maintenance_window,
        storage_encrypted=storage_encrypted,
        engine_mode=engine_mode,
        deletion_protection=deletion_protection,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_subnet_group_name=db_subnet_group_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")

    # Delete cluster with test flag
    ret = await hub.states.aws.rds.db_cluster.absent(
        test_ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        skip_final_snapshot=True,
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.rds.db_cluster '{db_cluster_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete cluster
    ret = await hub.states.aws.rds.db_cluster.absent(
        ctx, name=db_cluster_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.rds.db_cluster '{db_cluster_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.rds.db_cluster.absent(
        ctx, name=db_cluster_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert f"aws.rds.db_cluster '{db_cluster_name}' already absent" in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))


def assert_db_cluster(
    hub,
    resource,
    db_cluster_name,
    engine,
    master_username,
    backup_retention_period,
    database_name,
    engine_version,
    port,
    preferred_backup_window,
    preferred_maintenance_window,
    storage_encrypted,
    engine_mode,
    deletion_protection,
    copy_tags_to_snapshot,
    db_subnet_group_name,
    tags,
):
    assert db_cluster_name == resource.get("name")
    assert engine == resource.get("engine")
    assert master_username == resource.get("master_username")
    assert backup_retention_period == resource.get("backup_retention_period")
    assert database_name == resource.get("database_name")
    assert engine_version == resource.get("engine_version")
    assert port == resource.get("port")
    assert preferred_backup_window == resource.get("preferred_backup_window")
    assert preferred_maintenance_window == resource.get("preferred_maintenance_window")
    assert storage_encrypted == resource.get("storage_encrypted")
    assert engine_mode == resource.get("engine_mode")
    assert deletion_protection == resource.get("deletion_protection")
    assert copy_tags_to_snapshot == resource.get("copy_tags_to_snapshot")
    assert db_subnet_group_name == resource.get("db_subnet_group_name")
    assert tags == resource.get("tags")
