import json
import time
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_standard_queue(hub, ctx, aws_dead_letter_sqs_queue_arn):
    name = "idem-test-sqs-queue-" + str(int(time.time()))

    # The account id is the second element from the end of the ARN
    account_id = aws_dead_letter_sqs_queue_arn.split(":")[-2]
    test_queue_arn = f"arn:aws:sqs:{ctx.acct.region_name}:{account_id}:{name}"

    initial_attributes = {
        "name": name,
        "resource_id": None,
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": {
            "Version": "2012-10-17",
            "Id": "test-queue-policy-" + str(uuid.uuid4()),
            "Statement": [
                {
                    "Effect": "Deny",
                    "Principal": {"AWS": f"arn:aws:iam::{account_id}:root"},
                    "Action": "sqs:SendMessage",
                    "Resource": test_queue_arn,
                }
            ],
        },
        "receive_message_wait_time_seconds": 5,
        "redrive_policy": {
            "deadLetterTargetArn": aws_dead_letter_sqs_queue_arn,
            "maxReceiveCount": 3,
        },
        "visibility_timeout": 1579,
        "kms_master_key_id": "alias/aws/sqs",
        "kms_data_key_reuse_period_seconds": 125,
        "sqs_managed_sse_enabled": False,
        "tags": {"example": "abc"},
    }

    updated_attributes = {
        "name": name,
        "resource_id": None,
        "delay_seconds": 0,
        "maximum_message_size": 5321,
        "message_retention_period": 375,
        "policy": {
            "Version": "2012-10-17",
            "Id": "test-queue-policy-" + str(uuid.uuid4()),
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": f"arn:aws:iam::{account_id}:root"},
                    "Action": "sqs:SendMessage",
                    "Resource": test_queue_arn,
                }
            ],
        },
        "receive_message_wait_time_seconds": 5,
        "redrive_policy": {
            "deadLetterTargetArn": aws_dead_letter_sqs_queue_arn,
            "maxReceiveCount": 5,
        },
        "visibility_timeout": 555,
        "kms_master_key_id": "alias/sample/aws/queue/test",
        "kms_data_key_reuse_period_seconds": 195,
        "sqs_managed_sse_enabled": False,
        "tags": {"example": "abc"},
    }

    updated_tags = {"hello": "hey", "example": "xyz", "Company": "Test1"}

    await hub.tool.sqs_queue_test_util.assert_sqs_queue(
        ctx, initial_attributes, updated_attributes, updated_tags
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_fifo_queue(hub, ctx):
    name = f"idem-test-sqs-queue-{str(int(time.time()))}.fifo"

    initial_attributes = {
        "name": name,
        "resource_id": None,
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "receive_message_wait_time_seconds": 5,
        "visibility_timeout": 1579,
        "kms_master_key_id": "sample/aws/sqs",
        "kms_data_key_reuse_period_seconds": 125,
        "sqs_managed_sse_enabled": False,
        "fifo_queue": True,
        "content_based_deduplication": False,
        "deduplication_scope": "messageGroup",
        "fifo_throughput_limit": "perMessageGroupId",
        "tags": {"example": "abc"},
    }

    updated_attributes = {
        "name": name,
        "resource_id": None,
        "delay_seconds": 0,
        "maximum_message_size": 5321,
        "message_retention_period": 375,
        "receive_message_wait_time_seconds": 5,
        "visibility_timeout": 555,
        "kms_master_key_id": "sample/aws/sqs/queue/test",
        "kms_data_key_reuse_period_seconds": 195,
        "sqs_managed_sse_enabled": False,
        "fifo_queue": True,
        "content_based_deduplication": True,
        "deduplication_scope": "queue",
        "fifo_throughput_limit": "perQueue",
        "tags": {"example": "abc"},
    }

    # The attribute ContentBasedDeduplication, which applies only for FIFO queues,
    # does not work properly with Localstack,
    # which is why this attribute is used only with the real AWS.
    # This the Github issue https://github.com/localstack/localstack/issues/3615
    # and although it is closed, the issue still appears.
    if hub.tool.utils.is_running_localstack(ctx):
        initial_attributes.pop("content_based_deduplication")
        updated_attributes.pop("content_based_deduplication")

    updated_tags = {"hello": "hey", "example": "xyz", "Company": "Test1"}

    await hub.tool.sqs_queue_test_util.assert_sqs_queue(
        ctx, initial_attributes, updated_attributes, updated_tags
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_queue_policy_update(hub, ctx, aws_dead_letter_sqs_queue_arn):
    name = "idem-test-sqs-queue-" + str(int(time.time()))

    # The account id is the second element from the end of the ARN
    account_id = aws_dead_letter_sqs_queue_arn.split(":")[-2]
    test_queue_arn = f"arn:aws:sqs:{ctx.acct.region_name}:{account_id}:{name}"
    policy_id = "test-queue-policy-" + str(uuid.uuid4())
    policy = {
        "Version": "2012-10-17",
        "Id": policy_id,
        "Statement": [
            {
                "Effect": "Deny",
                "Principal": {"AWS": f"arn:aws:iam::{account_id}:root"},
                "Action": "sqs:SendMessage",
                "Resource": test_queue_arn,
            }
        ],
    }
    policy_str = json.dumps(policy)

    initial_attributes = {
        "name": name,
        "resource_id": None,
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": policy,
        "receive_message_wait_time_seconds": 5,
        "redrive_policy": {
            "deadLetterTargetArn": aws_dead_letter_sqs_queue_arn,
            "maxReceiveCount": 3,
        },
        "visibility_timeout": 1579,
        "kms_master_key_id": "alias/aws/sqs",
        "kms_data_key_reuse_period_seconds": 125,
        "sqs_managed_sse_enabled": False,
        "tags": {"example": "abc"},
    }

    updated_attributes = {
        "name": name,
        "resource_id": None,
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": policy_str,
        "receive_message_wait_time_seconds": 20,
        "redrive_policy": {
            "deadLetterTargetArn": aws_dead_letter_sqs_queue_arn,
            "maxReceiveCount": 3,
        },
        "visibility_timeout": 1579,
        "kms_master_key_id": "alias/aws/sqs",
        "kms_data_key_reuse_period_seconds": 125,
        "sqs_managed_sse_enabled": False,
        "tags": {"example": "abc"},
    }

    # create
    ret = await hub.states.aws.sqs.queue.present(ctx, **initial_attributes)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") is None
    assert ret.get("new_state")
    updated_attributes["resource_id"] = ret.get("new_state")["resource_id"]
    old_policy = ret["new_state"].get("policy")

    # update - only 'receive_message_wait_time_seconds' should be updated
    # Policy should not detect change even when passed as dict or str
    ret = await hub.states.aws.sqs.queue.present(ctx, **updated_attributes)
    assert ret["result"], ret["comment"]
    assert f"Updated aws.sqs.queue '{name}'" in ret["comment"]
    assert len(ret["changes"].get("old").keys()) == 1
    assert (
        ret["changes"].get("old").get("receive_message_wait_time_seconds") is not None
    )

    # update - policy
    new_policy = {
        "Version": "2012-10-17",
        "Id": policy_id,
        "Statement": [
            {
                "Effect": "Accept",
                "Principal": {"AWS": f"arn:aws:iam::{account_id}:root"},
                "Action": "sqs:SendMessage",
                "Resource": test_queue_arn,
            }
        ],
    }

    updated_attributes["policy"] = new_policy
    # update - only 'policy' should be updated
    ret = await hub.states.aws.sqs.queue.present(ctx, **updated_attributes)
    assert ret["result"], ret["comment"]
    assert f"Updated aws.sqs.queue '{name}'" in ret["comment"]
    assert len(ret["changes"].get("old").keys()) == 1
    assert ret["changes"].get("old").get("policy") is not None
    assert old_policy.get("Statement") == ret["changes"].get("old").get("policy").get(
        "Statement"
    )
    assert new_policy.get("Statement") == ret["changes"].get("new").get("policy").get(
        "Statement"
    )
