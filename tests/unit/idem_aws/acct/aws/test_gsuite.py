import io
import os
from unittest import mock

import pytest
from dict_tools.data import NamespaceDict

try:
    pass

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False


class mock_session:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


@pytest.mark.skipif(not HAS_LIBS, reason="aws_google_auth is not installed")
@pytest.mark.asyncio
async def test_gather(hub, mock_hub):
    mock_hub.acct.aws.gsuite.gather = hub.acct.aws.gsuite.gather
    mock_hub.tool.boto3.session.get = hub.tool.boto3.session.get

    mock_hub.acct.PROFILES = hub.acct.PROFILES
    mock_hub.acct.aws.gsuite.TMP = "tmp"
    # Don't load the saved credentials
    mock_hub.acct.aws.gsuite.load_profile.return_value = ({}, "")
    boto_creds = {
        "aws_access_key_id": "mock_id",
        "aws_secret_access_key": "mock_key",
        "aws_session_token": "mock_token",
        "region_name": "mock_region",
        "endpoint_url": "endpoint_url",
    }
    mock_hub.acct.aws.gsuite.parse_opts.return_value = (
        boto_creds,
        "2020-06-11T12:00:31+0000",
    )
    profile_name = "mock_profile_1"
    endpoint_url = "endpoint_url"
    duration = 0
    idp_id = "mock_idp"
    region = "mock_region"
    role_arn = "mock_role"
    sp_id = "mock_sp"
    resolve_aliases = "mock_aliases"
    username = "mock_username"
    account = "mock_account"
    keyring = "mock_keyring"
    saml_assertion = "mock_assertion"
    saml_cache = "mock_cache"
    bogus_kwarg = "this should be ignored"
    profiles = NamespaceDict()

    profiles["aws.gsuite"] = {
        profile_name: {
            "endpoint_url": endpoint_url,
            "duration": duration,
            "idp_id": idp_id,
            "region": region,
            "role_arn": role_arn,
            "sp_id": sp_id,
            "resolve_aliases": resolve_aliases,
            "username": username,
            "account": account,
            "keyring": keyring,
            "saml_assertion": saml_assertion,
            "saml_cache": saml_cache,
            "bogus_kwarg": bogus_kwarg,
        }
    }
    with mock.patch.object(hub.tool.boto3, "SESSION", mock_session):
        profiles = await mock_hub.acct.aws.gsuite.gather(profiles)

    mock_hub.acct.aws.gsuite.load_profile.assert_called_once_with(
        os.path.join(mock_hub.acct.aws.gsuite.TMP, "mock_profile_1.fernet")
    )
    mock_hub.acct.aws.gsuite.dump_profile.assert_called_once_with(
        os.path.join(mock_hub.acct.aws.gsuite.TMP, "mock_profile_1.fernet"),
        boto_creds,
        "2020-06-11T12:00:31+0000",
    )

    # Verify that the profile was created
    assert profile_name in profiles
    # Verify that endpoint_url and provider_tag_key became part of the profile
    assert profiles[profile_name]["endpoint_url"] == endpoint_url

    # Verify that all the expected kwargs are present
    kwargs = profiles[profile_name]
    assert kwargs["aws_access_key_id"] == boto_creds["aws_access_key_id"]
    assert kwargs["aws_session_token"] == boto_creds["aws_session_token"]
    assert kwargs["region_name"] == region
    assert kwargs["endpoint_url"] == boto_creds["endpoint_url"]

    # Verify that all the extra kwargs were consumed before they got passed to the session
    assert not kwargs.get("bogus_kwarg")


@pytest.mark.skipif(not HAS_LIBS, reason="aws_google_auth is not installed")
@pytest.mark.asyncio
async def test_gather_saved(hub, mock_hub):
    mock_hub.acct.aws.gsuite.gather = hub.acct.aws.gsuite.gather
    mock_hub.tool.boto3.session.get = hub.tool.boto3.session.get
    mock_hub.acct.PROFILES = hub.acct.PROFILES
    mock_hub.acct.aws.gsuite.TMP = "tmp"
    boto_creds = {
        "aws_access_key_id": "mock_id",
        "aws_secret_access_key": "mock_key",
        "aws_session_token": "mock_token",
        "region_name": "mock_region",
        "endpoint_url": "endpoint_url",
    }
    mock_hub.acct.aws.gsuite.load_profile.return_value = (
        boto_creds,
        "2020-06-11T12:00:31+0000",
    )
    profile_name = "mock_profile_1"
    endpoint_url = "endpoint_url"
    duration = 0
    idp_id = "mock_idp"
    region = "mock_region"
    role_arn = "mock_role"
    sp_id = "mock_sp"
    resolve_aliases = "mock_aliases"
    username = "mock_username"
    account = "mock_account"
    keyring = "mock_keyring"
    saml_assertion = "mock_assertion"
    saml_cache = "mock_cache"
    bogus_kwarg = "this should be ignored"
    profiles = {}

    profiles["aws.gsuite"] = {
        profile_name: {
            "duration": duration,
            "idp_id": idp_id,
            "region": region,
            "role_arn": role_arn,
            "sp_id": sp_id,
            "resolve_aliases": resolve_aliases,
            "username": username,
            "account": account,
            "keyring": keyring,
            "saml_assertion": saml_assertion,
            "saml_cache": saml_cache,
            "bogus_kwarg": bogus_kwarg,
        }
    }
    with mock.patch.object(hub.tool.boto3, "SESSION", mock_session):
        profiles = await mock_hub.acct.aws.gsuite.gather(profiles)

    mock_hub.acct.aws.gsuite.load_profile.assert_called_once_with(
        os.path.join(mock_hub.acct.aws.gsuite.TMP, "mock_profile_1.fernet")
    )

    # Verify that the profile was created
    assert profile_name in profiles
    assert profiles[profile_name]["endpoint_url"] == endpoint_url

    # Verify that all the expected kwargs are present
    kwargs = profiles[profile_name]
    assert kwargs["aws_access_key_id"] == boto_creds["aws_access_key_id"]
    assert kwargs["aws_session_token"] == boto_creds["aws_session_token"]
    assert kwargs["region_name"] == region
    assert kwargs["endpoint_url"] == boto_creds["endpoint_url"]

    # Verify that all the extra kwargs were consumed before they got passed to the session
    assert not kwargs.get("bogus_kwarg")


@pytest.mark.skipif(not HAS_LIBS, reason="aws_google_auth is not installed")
def test_parse_opts(hub):
    options = {
        "endpoint_url": "endpoint_url",
        "duration": 9999,
        "idp_id": "mock_idp",
        "region": "mock_region",
        "role_arn": "mock_role",
        "sp_id": "mock_sp",
        "resolve_aliases": "mock_resolve",
        "username": "mock_user",
        "account": "mock_account",
        "keyring": "mock_keyring",
        "saml_assertion": "mock_saml",
        "saml_cache": "mock_cache",
    }
    expiration = "2020-06-11T12:00:31+0000"
    token = "mock_aws_access_token"
    key = "mock_aws_acces_key"
    id_ = "mock_aws_access_id"
    mock_io = io.StringIO()
    mock_io.write(
        f"export AWS_ACCESS_KEY_ID='{id_}' AWS_SECRET_ACCESS_KEY='{key}'"
        f" AWS_SESSION_TOKEN='{token}' AWS_SESSION_EXPIRATION='{expiration}'"
    )

    with mock.patch("aws_google_auth.process_auth") as process_auth:
        with mock.patch("io.StringIO", return_value=mock_io):
            ctx, parsed_expiration = hub.acct.aws.gsuite.parse_opts(**options)
        process_auth.assert_called_once()

    # Verify that endpoint_url and provider_tag_key are get passed through all of this
    assert ctx["endpoint_url"] == options["endpoint_url"]
    assert ctx["aws_access_key_id"] == id_
    assert ctx["aws_secret_access_key"] == key
    assert ctx["aws_session_token"] == token
    assert ctx["region_name"] == options["region"]

    assert expiration == parsed_expiration
