PRESENT_ATTRIBUTES = {
    "delay_seconds": 55,
    "maximum_message_size": 5321,
    "message_retention_period": 175,
    "policy": {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["111111111111"]},
                "Action": "sqs:SendMessage",
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
            }
        ],
    },
    "receive_message_wait_time_seconds": 5,
    "redrive_policy": {
        "deadLetterTargetArn": "aws:sqs:queue:arn",
        "maxReceiveCount": 3,
    },
    "visibility_timeout": 1579,
    "kms_master_key_id": "sample/aws/sqs",
    "kms_data_key_reuse_period_seconds": 125,
    "sqs_managed_sse_enabled": False,
    "fifo_queue": True,
    "content_based_deduplication": True,
    "deduplication_scope": "message_group",
    "fifo_throughput_limit": "perMessageGroupId",
}

RAW_ATTRIBUTES = {
    "DelaySeconds": "55",
    "MaximumMessageSize": "5321",
    "MessageRetentionPeriod": "175",
    "Policy": '{"Version": "2012-10-17", "Id": "Test_Queue_Policy", "Statement": '
    '[{"Effect": "Allow", "Principal": {"AWS": ["111111111111"]}, "Action": '
    '"sqs:SendMessage", "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo"}]}',
    "ReceiveMessageWaitTimeSeconds": "5",
    "RedrivePolicy": '{"deadLetterTargetArn": "aws:sqs:queue:arn", '
    '"maxReceiveCount": 3}',
    "VisibilityTimeout": "1579",
    "KmsMasterKeyId": "sample/aws/sqs",
    "KmsDataKeyReusePeriodSeconds": "125",
    "SqsManagedSseEnabled": "False",
    "FifoQueue": "True",
    "ContentBasedDeduplication": "True",
    "DeduplicationScope": "message_group",
    "FifoThroughputLimit": "perMessageGroupId",
}

POLICY_STATEMENT_WITH_CONDITIONS_UNSORTED_LISTS = {
    "Effect": "Allow",
    "Principal": {"AWS": ["55555555555", "111111111111", "999999999999"]},
    "Action": ["sqs:SendMessage", "sqs:ReceiveMessage", "sqs:TagQueue"],
    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
    "Condition": {
        "DateGreaterThan": {"aws:EpochTime": "2022-08-09T00:00:01Z"},
        "StringEquals": {
            "aws:RequestTag/company": ["public", "private"],
            "aws:RequestTag/abs": ["xyz"],
            "aws:RequestTag/device": ["phone", "computer", "monitor"],
        },
        "ForAllValues:StringEquals": {
            "aws:TagKeys": ["cost-center", "abc", "environment"]
        },
    },
}
POLICY_STATEMENT_WITH_CONDITIONS_SORTED_LISTS = {
    "Effect": "Allow",
    "Principal": {"AWS": ["111111111111", "55555555555", "999999999999"]},
    "Action": ["sqs:ReceiveMessage", "sqs:SendMessage", "sqs:TagQueue"],
    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
    "Condition": {
        "ForAllValues:StringEquals": {
            "aws:TagKeys": ["abc", "cost-center", "environment"]
        },
        "StringEquals": {
            "aws:RequestTag/company": ["private", "public"],
            "aws:RequestTag/device": ["computer", "monitor", "phone"],
            "aws:RequestTag/abs": ["xyz"],
        },
        "DateGreaterThan": {"aws:EpochTime": "2022-08-09T00:00:01Z"},
    },
}


def test_convert_present_attributes_to_raw(hub, ctx):
    result = hub.tool.aws.sqs.conversion_utils.convert_present_attributes_to_raw(
        **PRESENT_ATTRIBUTES
    )
    assert result == RAW_ATTRIBUTES


def test_convert_raw_attributes_to_present(hub):
    result = hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present(
        RAW_ATTRIBUTES
    )
    assert result == PRESENT_ATTRIBUTES


def test_compare_present_queue_attributes(hub, ctx):

    expected_attributes = {
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                }
            ],
        },
        "receive_message_wait_time_seconds": 5,
        "redrive_policy": {
            "deadLetterTargetArn": "aws:sqs:queue:arn",
            "maxReceiveCount": 3,
        },
    }

    assert hub.tool.aws.sqs.queue_utils.compare_present_queue_attributes(
        expected_attributes, PRESENT_ATTRIBUTES
    )

    expected_attributes = {"delay_seconds": 15}
    present_attributes = {
        "delay_seconds": 15,
        "maximum_message_size": 262144,
        "message_retention_period": 345600,
        "receive_message_wait_time_seconds": 0,
        "visibility_timeout": 30,
        "sqs_managed_sse_enabled": False,
    }

    assert hub.tool.aws.sqs.queue_utils.compare_present_queue_attributes(
        expected_attributes, present_attributes
    )

    expected_attributes = {
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                },
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["55555555555", "111111111111"]},
                    "Action": "sqs:ReceiveMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                },
                POLICY_STATEMENT_WITH_CONDITIONS_SORTED_LISTS,
            ],
        },
        "redrive_policy": {
            "deadLetterTargetArn": "aws:sqs:queue:arn",
            "maxReceiveCount": 3,
        },
    }
    present_attributes = {
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111", "55555555555"]},
                    "Action": "sqs:ReceiveMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                },
                POLICY_STATEMENT_WITH_CONDITIONS_UNSORTED_LISTS,
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                },
            ],
        },
        "redrive_policy": {
            "maxReceiveCount": 3,
            "deadLetterTargetArn": "aws:sqs:queue:arn",
        },
    }

    assert hub.tool.aws.sqs.queue_utils.compare_present_queue_attributes(
        expected_attributes, present_attributes
    )

    expected_attributes = {
        "delay_seconds": 55,
        "maximum_message_size": 5321,
        "message_retention_period": 175,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                }
            ],
        },
        "redrive_policy": {
            "deadLetterTargetArn": "aws:sqs:queue:arn",
            "maxReceiveCount": 3,
        },
    }
    present_attributes = {
        "delay_seconds": 55,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                }
            ],
        },
        "redrive_policy": {
            "maxReceiveCount": 3,
            "deadLetterTargetArn": "aws:sqs:queue:arn",
        },
    }

    assert not hub.tool.aws.sqs.queue_utils.compare_present_queue_attributes(
        expected_attributes, present_attributes
    )

    expected_attributes = {
        "delay_seconds": 55,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Deny",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                }
            ],
        },
        "redrive_policy": {
            "deadLetterTargetArn": "aws:sqs:queue:arn",
            "maxReceiveCount": 3,
        },
    }
    present_attributes = {
        "delay_seconds": 55,
        "policy": {
            "Version": "2012-10-17",
            "Id": "Test_Queue_Policy",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": ["111111111111"]},
                    "Action": "sqs:SendMessage",
                    "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                }
            ],
        },
        "redrive_policy": {
            "maxReceiveCount": 3,
            "deadLetterTargetArn": "aws:sqs:queue:arn",
        },
    }

    assert not hub.tool.aws.sqs.queue_utils.compare_present_queue_attributes(
        expected_attributes, present_attributes
    )


def test_are_policies_equal(hub, ctx):
    expected_policy = {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["111111111111"]},
                "Action": "sqs:SendMessage",
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
            },
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["55555555555", "111111111111"]},
                "Action": "sqs:ReceiveMessage",
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
            },
        ],
    }

    actual_policy = {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["111111111111", "55555555555"]},
                "Action": "sqs:ReceiveMessage",
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
            },
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["111111111111"]},
                "Action": "sqs:SendMessage",
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
            },
        ],
    }

    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        expected_policy, actual_policy
    )

    expected_policy = {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["111111111111", "55555555555", "999999999999"]},
                "Action": ["sqs:ReceiveMessage", "sqs:SendMessage", "sqs:TagQueue"],
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                "Condition": {
                    "ForAllValues:StringEquals": {
                        "aws:TagKeys": ["abc", "cost-center", "environment"]
                    }
                },
            }
        ],
    }

    actual_policy = {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["55555555555", "111111111111", "999999999999"]},
                "Action": ["sqs:SendMessage", "sqs:ReceiveMessage", "sqs:TagQueue"],
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
                "Condition": {
                    "ForAllValues:StringEquals": {
                        "aws:TagKeys": ["environment", "cost-center", "abc"]
                    }
                },
            }
        ],
    }

    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        expected_policy, actual_policy
    )

    expected_policy = {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [POLICY_STATEMENT_WITH_CONDITIONS_SORTED_LISTS],
    }

    actual_policy = {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [POLICY_STATEMENT_WITH_CONDITIONS_UNSORTED_LISTS],
    }

    assert hub.tool.aws.state_comparison_utils.are_policies_equal(
        expected_policy, actual_policy
    )
